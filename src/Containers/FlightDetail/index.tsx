import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Alert, ScrollView, Image } from 'react-native'
import styles from './style'
import { BoxShadow } from 'react-native-shadow';
export default class FlightDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    } render() {
        const { origin, destination, images, price } = this.props;
        return (
            <ScrollView>
                <View style={styles.container}>
                    <Text> {this.props.origin.iata}</Text>
                    <Text> {this.props.destination.iata}</Text>
                    <Text> {this.props.origin.city}</Text>
                    <Text> {this.props.origin.country}</Text>
                    <View style={styles.iconContStyle}>
                        <Image source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRqTQ13uEt5zYhjzic-65KAheLKY6rHt3Bnw49je6M5P7T3Zn_Q' }}
                            //  <Image source={{ uri: images }}
                            style={{ width: 300, height: 300 }} />
                    </View>
                    <View style={{ flexDirection: 'row', }}>
                        <View style={styles.leftContStyle}>
                            <Text style={styles.bodyTStyle}>Ciudad:</Text>
                            <Text style={styles.bodyTStyle}>pais:</Text>
                            <Text style={styles.bodyTStyle}>perecio:</Text>
                        </View>
                        <View style={styles.rContStyle}>
                            <Text> {this.props.destination.city}</Text>
                            <Text> {this.props.destination.country}</Text>
                            <Text> {this.props.price}</Text>
                        </View>
                    </View>
                    <BoxShadow setting={styles.shadowFeat}>
                        <TouchableOpacity
                            activeOpacity={1}
                            style={styles.button}
                            onPress={() => Alert.alert('Se compro el vuelo')} >
                            <Text style={styles.text}>{'COMPRAR'}</Text>
                        </TouchableOpacity>
                    </BoxShadow>
                </View>
            </ScrollView>
        )
    }
}