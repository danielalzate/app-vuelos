import React, { Component } from 'react';
import { Text, TouchableOpacity, View, FlatList, StyleSheet, ActivityIndicator } from 'react-native';
import { getFlightRoundTrip } from '../../api/'
import ButtonCityCard from '../../Components/ButtonCityCard'
export default class FlightListRound extends Component {
  constructor(props) {
    super(props)
    this.state = {
      oRoutes: []


    }
  }
  async componentWillMount() {
    this.props
    let flights = await getFlightRoundTrip(this.props.text1, this.props.text2)
    this.setState({
      oRoutes: flights
    })
  }
  render() {
    console.log('STYAT', this.state);

    return (
      <View style={oRouteStyle.container}>
      <Text>{'IDA'}</Text>
      <View style={oRouteStyle.container}>
        {!this.state.oRoutes.go && <ActivityIndicator size="large" />}
        {this.state.oRoutes.go && <FlatList
          data={this.state.oRoutes.go}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => <ButtonCityCard flight={item} />
          } />}
      </View>
      <Text>{'VUELTA'}</Text>
      <View style={oRouteStyle.container}>
        {!this.state.oRoutes.back && <ActivityIndicator size="large" />}
        {this.state.oRoutes.back && <FlatList
          data={this.state.oRoutes.back}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => <ButtonCityCard flight={item} />
          } />}
      </View>
      </View>
    );
  }
}

/** Styles */
const oRouteStyle = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    //alignItems: 'center',
  },
  text: {
    height: 200,
  }

});
