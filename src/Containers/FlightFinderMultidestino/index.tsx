import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ViewStyle, Picker } from 'react-native'
import styles from './style'
import * as Animatable from 'react-native-animatable'
import { animation } from '../../Helpers/Animated'
import { BoxShadow } from 'react-native-shadow';
import Search from '../../Components/Search'
import { Actions } from 'react-native-router-flux'
import DatePicker from '../../Components/DatePicker'
import { getAllAirports } from '../../api/'
export default class FlightFinderMultidestino extends Component {
    bntAnimation: any
    constructor(props) {
        super(props)
        this.state = {
            origin:{ iata: '-',
             city: '-',
              country: '-' },
              destination:{ iata: '-',
             city: '-',
              country: '-' },
            chosenDate: '-',
            flightsArray:[],
            services:[{i:'a',l:'a'},{i:'b',l:'b'},{i:'c',l:'a'}],
            selectedValue:'',
            selectedService:''
        }
        animation()
        this.date = this.date.bind(this);
    }
    btnSelectRoom() {
        this.bntAnimation.zoomOut()
        Actions.FlightList()
    }
    date(a) {
        this.setState({ chosenDate: a })
    }
    addFlight(){
        this.state.flightsArray.push({ 'origen':{}, 'destination':{}, 'date':'' } );
        this.setState({flightsArray:this.state.flightsArray})
    }
    async componentWillMount() {
        let airports = await getAllAirports()
        this.setState({
            services: airports
        })
        console.log(airports)
      }
    render() { 
        const {origin,destination}=this.state
        return (
            <View style={styles.container}>
                <Search origin={ origin}
                    destination={destination} date={'-'} />
                <Search origin={{ iata: '.', city: '-', country: '-' }}
                    destination={{ iata: '-', city: '-', country: '' }} date={this.state.chosenDate} />
                <DatePicker event={this.date} ></DatePicker>
                <BoxShadow setting={styles.shadowFeat}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={styles.button}
                        onPress={() => this.btnSelectRoom()} >
                        <Animatable.View
                            ref={ref => this.bntAnimation = ref}
                            duration={1500}
                            style={styles.circule as ViewStyle} />
                        <Text style={styles.text}>{'BUSCAR'}</Text>
                    </TouchableOpacity>
                </BoxShadow>
                <Picker 
                 style={{ height: 50, width: 300 }}
                    selectedValue={this.state.selectedService}
                    onValueChange={(service) => this.setState({selectedService:service})}>
                 {this.state.services.map((city, iata) => {return <Picker.Item value={city} label={"Cliente " + iata} key={iata}  /> })}
                </Picker>
            </View>
        )
    }
}