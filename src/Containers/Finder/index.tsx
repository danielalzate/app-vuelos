import React, { Component } from 'react';
import { Text, TouchableOpacity, View, FlatList, StyleSheet } from 'react-native';
import { getAllAirports } from '../../api/'
import ButtonAirpot from '../../Components/ButtonAirpot'
export default class Finder extends Component {
  constructor(props) {
    super(props)
    this.state = {
      oRoutes: []
    }
  }
  async componentWillMount() {
    let airports = await getAllAirports()
    this.setState({
      oRoutes: airports
    })
    console.log(airports)
  }

  render() {
    return (
      <View style={oRouteStyle.container}>
        {/* {!this.state.oRoutes && <ActivityIndicator size="large"/>} */}
        {this.state.oRoutes && <FlatList
          data={this.state.oRoutes}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => <ButtonAirpot flight={item} />
          } />}
      </View>

    );
  }

}

/** Styles */
const oRouteStyle = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  },
  text: {
    height: 200,
  }

});
