import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    container: {
        backgroundColor: 'rgba(248,248,248,1.0)',
        alignItems: 'center',
        height: 512,
        justifyContent: 'center'
    },
    text: {
        height: 200,
    },
    container1: {
        width: 144,
        height: 80,
        borderRadius: 2,
        backgroundColor: 'rgba(255, 255, 255, 1)',
        alignItems: 'center',
        justifyContent: 'center',
    }
})
