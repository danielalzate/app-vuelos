import React, { Component } from 'react';
import { Text, TouchableOpacity, View, FlatList, StyleSheet, ActivityIndicator } from 'react-native';
import { getFlightOneway } from '../../api/'
import ButtonCityCard from '../../Components/ButtonCityCard'
import styles from './style'
export default class FlightList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      oRoutes: null
    }
  }
  async componentWillMount() {
    let flights = await getFlightOneway(this.props.text1, this.props.text2)
    this.setState({
      oRoutes: flights
    })
  }
  render() {
    return (
      <View style={styles.container}>
        {!this.state.oRoutes && <ActivityIndicator size="large" />}
        {this.state.oRoutes && <FlatList
          data={this.state.oRoutes}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => <ButtonCityCard flight={item} />
          } />}
      </View>
    );
  }
}