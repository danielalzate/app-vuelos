import React, { Component } from 'react'
import { Text, View, TouchableOpacity, TextInput, Picker } from 'react-native'
import styles from './style'
import { BoxShadow } from 'react-native-shadow';
import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome';
import { getAllAirports } from '../../api/'
import value from '*.json';
export default class FlightFinderOneway extends Component {
    constructor(props) {
        super(props);
        this.state = { text1: '', text2: '', text3: '',
    selected:'mit' };
    }
    btnSelectRoom() {
        Actions.FlightList(this.state);
    }
    async componentWillMount() {
        //let hotels = await getFligths(this.props.text1, this.props.text2)
        let flights = await getAllAirports()
        this.setState({
          oRoutes: flights
        })
      }
    render() {
        const { text1, text2, text3 } = this.state
        return (
            <View style={styles.container}>
                <View style={styles.vuelos}>
                    <BoxShadow setting={styles.shadowFeat1}>
                        <View style={styles.container1}>
                            <Text>{'Salgo de'}</Text>
                            <TextInput placeholder={'Ingrese una ciudad'}
                                style={{ height: 40, width: 130, borderWidth: 0 }}
                                onChangeText={(text) => this.setState({ text1: text })}
                                value={text1} />
                        </View>
                    </BoxShadow>
                    <BoxShadow setting={styles.shadowFeat1}>
                        <View style={styles.container1}>
                            <Text>{'Voy a'}</Text>
                            <TextInput placeholder={'Ingrese una ciudad'}
                                style={{ height: 40, width: 130, borderWidth: 0 }}
                                onChangeText={(text) => this.setState({ text2: text })}
                                value={text2} />
                        </View>
                    </BoxShadow>
                </View>
                <BoxShadow setting={styles.shadowFeat2}>
                    <View style={styles.container2}>
                        <Text>{'Fecha de salida'}</Text>
                        <Icon style={styles.icon2} name={'rocket'} size={30} color={'rgba(159, 162, 162, 1)'} />

                        <TextInput placeholder={'Ingrese una fecha'}
                            style={{ height: 35, width: 130, borderWidth: 0 }}
                            onChangeText={(text) => this.setState({ text3: text })}
                            value={text3} />
                    </View>
                </BoxShadow>
                <BoxShadow setting={styles.shadowFeat}>
                    <TouchableOpacity
                        activeOpacity={1}
                        style={styles.button}
                        onPress={() => this.btnSelectRoom()} >
                        <Text style={styles.text}>{'BUSCAR'}</Text>
                    </TouchableOpacity>
                </BoxShadow>
            </View>
        )
    }
}