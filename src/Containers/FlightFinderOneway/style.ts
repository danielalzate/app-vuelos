import { getComponentStyle } from '../../Helpers/Stylus'
export default getComponentStyle({
    container: {
        backgroundColor: 'rgba(248,248,248,1.0)',
        alignItems: 'center',
        height: 512
    },
    container1: {
        width: 144,
        height: 80,
        borderRadius: 2,
        backgroundColor: 'rgba(255, 255, 255, 1)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    vuelos: {
        width: 328,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 24,
        marginBottom:1 ,
    },
    button: {
        width: 328,
        height: 40,
        borderRadius: 2,
        backgroundColor: "#ea6422"
    },
    icon: {
        fontSize: 24
    },
    icon2: {
        position: 'absolute',
        left: 15,
        marginTop: 10,
        fontSize: 22
    },
    long: {
        marginTop: 16
    },
    text: {
        fontFamily: "ProximaNova",
        fontSize: 14,
        fontWeight: "600",
        fontStyle: "normal",
        lineHeight: 20,
        letterSpacing: 0,
        textAlign: "center",
        color: "#ffffff",
        marginTop: 10,
    },
    barText: {
        marginLeft: 37,
        marginTop: 16,

        fontSize: 20,
        fontStyle: "normal",
        lineHeight: 24,
        letterSpacing: 0,
        textAlign: "left",
        color: "#ffffff"
    },
    tabText: {

        fontSize: 13,
   
        letterSpacing: 0,
        textAlign: "center",
        color: "#ffffff"
    },
    circule: {
        width: 50,
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 150,
        position: 'absolute',
        opacity: 0,
        backgroundColor: "rgba(255, 255, 255,0.2)"
    },
    shadowFeat: {
        width: 328,
        height: 40,
        color: '#000',
        border: 2,
        radius: 3,
        opacity: 0.04,
        x: 0,
        y: 3,
        style: { marginVertical: 1, marginTop: 15 }
    },
    shadowFeat1: {
        width: 144,
        height: 80,
        color: '#000',
        border: 2,
        radius: 3,
        opacity: 0.04,
        x: 0,
        y: 3,
        style: { marginVertical: 1, marginTop: 0 }
    },
    shadowFeat2: {
        width: 328,
        height: 50,
        color: '#000',
        border: 2,
        radius: 3,
        opacity: 0.04,
        x: 0,
        y: 3,
        style: { marginVertical: 0, marginTop: 15 }
    },
    container2: {
        width: 328,
        height: 50,
        borderRadius: 2,
        backgroundColor: 'rgba(255, 255, 255, 1)',
      //  flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    dynamicCard: {
        borderTopWidth: 1,
        backgroundColor: "#ffffff",
        borderColor: 'rgba(0, 0, 0,0.2)',
        width: 360,
        height: 400,
        marginTop: 24
    },
    tabBar: {
        width: 360,
        height: 48,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    tab: {
        width: 120,
        height: 48,
        justifyContent: 'center'
    }
})