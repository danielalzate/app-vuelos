import React from 'react'
import { StyleSheet, Text, PixelRatio } from 'react-native'
import {
    Scene,
    Router,
    Actions,
    Tabs,
    Stack
} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome';
import TabPreview from '../Containers/Tabs/TabPreview'
import DatePicker from '../Components/DatePicker'
import FlightFinderOneway from '../Containers/FlightFinderOneway'
import FlightFinderRoundTrip from '../Containers/FlightFinderRoundTrip'
import FlightFinderMultidestino from '../Containers/FlightFinderMultidestino'
import Finder from '../Containers/Finder'
import FlightList from '../Containers/FlightList'
import FlightDetail from '../Containers/FlightDetail'
import FlightListRound from '../Containers/FlightListRound'
const AlmundoRouter = () => (
    <Router>
        <Scene key={'root'}>
            <Tabs
                key='tabbar'
                swipeEnabled={true}
                animationEnabled={true}
                showLabel={true}
                tabBarStyle={styles.tabBarStyle}
                activeTintColor='white'
                inactiveTintColor='rgba(35,40, 0, 0.5)'
                tabBarPosition={'top'}
                hideNavBar={true}>
                <Stack
                    key='tab1'
                    title='SOLO IDA'
                    inactiveBackgroundColor='blue'
                    activeBackgroundColor='bllue'
                    iconName={'back'}
                    iconSize={30}
                    icon={<Icon name={'rocket'} size={30} color={'rgba(159, 162, 162, 1)'} />}
                    initial={true}
                    navigationBarStyle={{ backgroundColor: 'green' }}
                    titleStyle={{ color: 'white', alignSelf: 'center' }}>
                    <Scene
                        key='tab1'
                        component={FlightFinderOneway}
                        title='Tab #1_1'
                        rightTitle='Right'
                        hideNavBar={true} />
                </Stack>
                <Stack
                    key='tab2'
                    title='IDA Y VUELTA'
                    iconName={'VR'}
                    iconSize={30}
                    icon={Icon}>
                    <Scene
                        key='tab_2_1'
                        component={FlightFinderRoundTrip}
                        title='Tab #2_1'
                        renderRightButton={() => <Text>Right</Text>}
                        hideNavBar={true} />
                </Stack>
                <Stack
                    key='tab_3'
                    title=' Multidestino'
                    iconName={'VR'}
                    iconSize={30}
                    icon={Icon}>
                    <Scene
                        key='tab_5'
                        component={FlightFinderMultidestino}
                        title='Tab #2_1'
                        renderRightButton={() => <Text>Right</Text>}
                        hideNavBar={true} />
                </Stack>
            </Tabs>
            <Scene key="DatePicker" component={DatePicker} title="Calendar" />
            <Scene key="Finder" component={Finder} title="Ciudades" />
            <Scene key="FlightList" component={FlightList} title="Vuelos" />
            <Scene key="FlightDetail" component={FlightDetail} title="Detalle" />
            <Scene key="FlightListRound" component={FlightListRound} title="Vuelos ida y vuelta" />
        </Scene>
    </Router>
)
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    tabBar: {
        borderTopColor: 'darkgrey',
        borderTopWidth: 2 / PixelRatio.get(),
        backgroundColor: '#d32f2f',
        opacity: 0.98
    },
    navigationBarStyle: {
        backgroundColor: 'blue'
    },
    navigationBarTitleStyle: {
        color: 'white'
    },
    tabBarStyle: {
        backgroundColor: '#d32f2f'
    },
    tabBarSelectedItemStyle: {
        backgroundColor: 'blue'
    }
})
export default AlmundoRouter