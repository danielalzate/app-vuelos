import { getComponentStyle } from '../../Helpers/Stylus'

export default getComponentStyle({
    container: {
        width: 162,
        height: 40,
        borderRadius: 2,
        backgroundColor: 'rgba(255, 255, 255, 1)',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    texto: {
        fontSize: 16,
        textAlign: "center",
        color: "#2e3030",
        lineHeight: 20,
        letterSpacing: 0,
    },
    icon: {
        position: 'absolute',
        left: 15,
        marginTop: 10,
        fontSize: 22
    },
    circule: {
        width: 50,
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        opacity: 0,
        backgroundColor: "rgba(100, 100, 100,0.2)"
    },
    shadowFeat: {
        width: 162,
        height: 40,
        color: '#000',
        border: 2,
        radius: 3,
        opacity: 0.04,
        x: 0,
        y: 3,
        style: { marginVertical: 0, marginTop: 15 }
    }
}) 