import React, { Component } from 'react'
import { Text, TouchableOpacity, ViewStyle } from 'react-native'
import styles from './style'
import { BoxShadow } from 'react-native-shadow';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable'
import { animation } from '../../Helpers/Animated'
import { Actions } from 'react-native-router-flux'
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
export default class Date extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isVisible: false,
            chosenDate: ''
        }
    }
    handlePicker = (datetime) => {
        this.setState({
            isVisible: false,
            chosenDate: moment(datetime).format('DD/MM/YYYY')
        })
    }
    showPicker = () => {
        this.setState({
            isVisible: true
        })
    }
    hidePicker = () => {
        this.setState({
            isVisible: false
        })
    }
    render() {
        animation()
        let bntAnimation: any
        const handlerButtonAnimation = ref => bntAnimation = ref
        function btnSelectRoom() {
            bntAnimation.zoomOut()
        }
        return (
            <BoxShadow setting={styles.shadowFeat}>
                <TouchableOpacity
                    activeOpacity={1}
                    style={styles.container}
                    onPress={() => btnSelectRoom()}>
                    <Animatable.View
                        ref={handlerButtonAnimation}
                        duration={1500}
                        style={styles.circule as ViewStyle} />
                    <Icon name={this.props.icon} size={30} color={'rgba(159, 162, 162, 1)'} />
                    <Text style={styles.texto}>{this.props.text}</Text>
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.isVisible}
                    onConfirm={this.handlePicker}
                    onCancel={this.hidePicker} />
            </BoxShadow>
        );
    }
}


