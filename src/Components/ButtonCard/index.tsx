import React, { Component } from 'react';
import { Text, TouchableOpacity, ViewStyle } from 'react-native'
import styles from './style'
import { BoxShadow } from 'react-native-shadow';
import * as Animatable from 'react-native-animatable'
import { animation } from '../../Helpers/Animated'
import { Actions } from 'react-native-router-flux'
export default ({ title, subtitle, text }) => {
    animation()
    let bntAnimation: any
    const handlerButtonAnimation = ref => bntAnimation = ref
    function btnSelectRoom() {
        bntAnimation.zoomOut()
        Actions.Finder()
    }
    return (
        <BoxShadow setting={styles.shadowFeat}>
            <TouchableOpacity
                activeOpacity={1}
                style={styles.container}
                onPress={() => btnSelectRoom()}>
                <Animatable.View
                    ref={handlerButtonAnimation}
                    duration={1000}
                    style={styles.circule as ViewStyle} />
                <Text style={styles.titulo}>{title}</Text>
                <Text style={styles.subtitulo1}>{subtitle}</Text>
                <Text style={styles.subtitulo2}> {text} </Text>
            </TouchableOpacity>
        </BoxShadow>
    )
}