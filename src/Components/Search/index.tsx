import React from 'react'
import { View, ViewStyle } from 'react-native'
import styles from './style'
//import { BoxShadow } from 'react-native-shadow';
//import * as Animatable from 'react-native-animatable'
//import { animation } from '../../Helpers/Animated'
import Card from '../../Components/ButtonCard'
import LongCard from '../../Components/LongButtonCard'
//import { Icon } from '../../Helpers/Icons';
export default ({ origin, destination, date }) => {
    return (
        <View style={styles.container}>
            <View style={styles.vuelos}>
                <Card
                    title={origin.iata}
                    subtitle={origin.city}
                    text={origin.country} />
                <Card
                    title={destination.iata}
                    subtitle={destination.city}
                    text={destination.country} />
            </View>
            <LongCard icon='rocket' text={date} />
        </View>



    )
}

