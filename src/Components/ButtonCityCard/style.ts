
import { getComponentStyle } from '../../Helpers/Stylus';
export default getComponentStyle({
    container: {
        width: 328,
        height: 80,
        borderRadius: 2,
        backgroundColor: 'rgba(255, 255, 255, 1)',
        alignItems: 'center',
        justifyContent: 'center', 
    },
    texto:{
        backgroundColor: 'rgba(255, 255, 255, 1)',
        height: 80,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        flexDirection: 'row',
    },
    shadowFeat: {
        width: 328,
        height: 80,
        color: '#000',
        border: 2,
        radius: 3,
        opacity: 0.04,
        x: 0,
        y: 3,
        style: { marginVertical: 0, marginTop: 10 }
    },
    titulo: {
        fontSize: 26,
        lineHeight: 24,
        textAlign: "center",
        color: 'rgba(46, 48, 48, 1)',
        marginTop: 6,
    },
    subtitulo1: {
        fontFamily: "ProximaNova",
        fontSize: 12,
        fontWeight: "600",
        fontStyle: "normal",
        lineHeight: 14,
        textAlign: "center",
        color: 'rgba(46, 48, 48, 1)',
        marginTop: 5,
    },
    subtitulo2: {
        fontSize: 12,
        fontWeight: "normal",
        fontStyle: "normal",
        textAlign: "center",
        marginTop: -1,
        color: 'rgba(46, 48, 48, 1)',
    },
    circule: {
        width: 20,
        height: 20,
        borderRadius: 30,
        justifyContent: 'center',
        position: 'absolute',
        opacity: 0,
        backgroundColor: "rgba(100, 100, 100,0.2)"
    },
})