import React, { Component } from 'react';
import { Text, TouchableOpacity, ViewStyle, View } from 'react-native'
import styles from './style'
import { BoxShadow } from 'react-native-shadow';
import { Actions } from 'react-native-router-flux'
export default ({ flight }) => {
    return (
        <BoxShadow setting={styles.shadowFeat}>
            <TouchableOpacity
                style={styles.texto}
                onPress={() => Actions.FlightDetail(flight)}>
                <View>
                    <Text style={styles.titulo}>{flight.origin.iata}</Text>
                    <Text style={styles.subtitulo1}>{flight.origin.city}</Text>
                    <Text style={styles.subtitulo2}> {flight.origin.country} </Text>
                </View>
                <View>
                    <Text style={styles.titulo}>{flight.destination.iata}</Text>
                    <Text style={styles.subtitulo1}>{flight.destination.city}</Text>
                    <Text style={styles.subtitulo2}> {flight.destination.country} </Text>
                </View>
            </TouchableOpacity>
        </BoxShadow>

    )
}