import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native'
import styles from './style'
import { BoxShadow } from 'react-native-shadow';
import { Actions } from 'react-native-router-flux'
export default ({ flight }) => {
    return (
        <BoxShadow setting={styles.shadowFeat}>
            <TouchableOpacity
                style={styles.texto}
                onPress={() => Actions.pop()}>
                <View>
                    <Text style={styles.titulo}>{flight.iata}</Text>
                    <Text style={styles.subtitulo1}>{flight.city}</Text>
                    <Text style={styles.subtitulo2}> {flight.country} </Text>
                </View>
            </TouchableOpacity>
        </BoxShadow>

    )
}