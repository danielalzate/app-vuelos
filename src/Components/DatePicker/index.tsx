import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import styles from './style'
export default class DatePicker extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isVisible: false,
            chosenDate: ''
        }
    }
    handlePicker = (datetime) => {
        this.setState({
            isVisible: false,
            chosenDate: moment(datetime).format('DD/MM/YYYY')
        })
        this.props.event(this.state.chosenDate)
    }
    showPicker = () => {
        this.setState({
            isVisible: true
        })
    }
    hidePicker = () => {
        this.setState({
            isVisible: false
        })
    }
    render() {
        return (
            <View style={styles.flex}>
                <TouchableOpacity style={styles.botton} onPress={this.showPicker}>
                    <Text>Calensario</Text>
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.isVisible}
                    onConfirm={this.handlePicker}
                    onCancel={this.hidePicker}/>
            </View>
        );
    }
}