// const ip = '10.0.3.152';
// const port = '5056';
const ngrok = 'f07feeb7.ngrok.io';

export const getAllAirports =
    async () => {
        const url = `http://${ngrok}/allAirports`
        const response = await fetch(url)
        let AllAirports = await response.json()
        return AllAirports
    }
export const getAllFligths =
    async () => {
        const url = `http://${ngrok}/allflights`
        const response = await fetch(url)
        let AllFligth = await response.json()
        return AllFligth
    }
export const getFlightOneway =
    async (origin, destination) => {
        const url = `http://${ngrok}/flights?origin=${origin}&destination=${destination}`
        const response = await fetch(url)
        let AllFligth = await response.json()
        return AllFligth
    }
export const getFlightRoundTrip =
    async (origin, destination) => {
        const url = `http://${ngrok}/flightRoundTrip?origin=${origin}&destination=${destination}`
        const response = await fetch(url)
        let AllFligth = await response.json()
        return AllFligth
    }
export const getFlightMulti =
    async (origin, destination) => {
        const url = `http://${ngrok}/flightRoundTrip?origin=${origin}&destination=${destination}`
        const response = await fetch(url)
        console.log(response);

        let AllFligth = await response.json()
        return AllFligth
    }