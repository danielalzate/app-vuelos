# App Vuelos

## Que es

App vuelos es una aplicacion mobil desarrollada en react-native para cumplir con la prueba del semillero almundo.


## Cómo ejecutarlo

Se inicia instalando las dependencias de Node usando el administrador de paquetes homebrew. Al instalar node se instaló npm, con este se instala la Interfaz de línea de comando de React Native la cual permite ejecutar las directrices para inicializar el proyecto y posteriormente ejecutarlo.

```
 npm install -g react-native-cli
```

Para visualizar los cambios en el emulador al guardar se instala watchman con el siguiente comando  
```
 brew install watchman
```

Luego, ir al folder appVuelos e instalar las dependencias del proyecto
```
npm install
```

Despues de esto, se puede probar la aplicación ejecutando el siguiente comando: 
```
 react-native run-android
```

## Librerías Usadas

Para crear la aplicación se usaron las siguientes librerías:
*react-native-animatable
*react-native-modal-datetime-picker
*react-native-shadow
*react-native-vector-icons
*react-native-router-flux

## DEMO
https://ibb.co/cJzRrx
https://ibb.co/micCWx
https://ibb.co/h3ZgPH